import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import  MyApp from "./MyApp"
import Footer from "./footer";
ReactDOM.render(
<div>
    <App />
    <MyApp/>
    <Footer/>
</div>

,
  document.getElementById('root')
);
